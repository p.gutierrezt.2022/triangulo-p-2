#!/usr/bin/env pythoh3

'''
Program to produce a triangle with numbers
'''

import sys


def line(number: int):
    """Return a string corresponding to the line for number"""
    valor=str(number)*number
    return valor

def triangle(number: int):
    """Return a string corresponding to the triangle for number"""
    valor=""

    try:
        if number <= 9:
            for a in range(1, number+1):
                valor = valor + line(a) + '\n'
            return valor
        else:
            raise ValueError
    except ValueError:
        number=9
        for a in range(1, number+1):
            valor = valor + line(a) + '\n'
        return valor

def main():
    number: int = sys.argv[1]
    try:
        valor = triangle(int(number))
    except ValueError:
        valor = triangle(9)
    print(valor, end="")

if __name__ == '__main__':
    main()